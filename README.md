# Model_Validation

A repository to share common validation and model metric tools.

Each directory contains a set python scripts and modules separated by topic.

model_pipeline.py contains a sample model pipeline that can efficiently process mutliple models.  This is not necessary to use the validation tools, but take a look and see if it can help with your analysis pipeline.

