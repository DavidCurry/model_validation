#####################################################
# cm_model_metrics.py
# Creates a table of confusion matrix metrics for mutiple models
#
# Last edited by David Curry on June 18th, 2018
####################################################


def model_metrics_classification(self):
    ''' Takes in a dict of models and creates a metric table'''
    
    # DF used for table visualizing
     metric_df = pd.DataFrame(index=[list(self.model_dict.keys())],
                              columns=['f1', 'precision', 'accuracy', 'recall'])

     f1, prec, acc, rec = [], [], [], []
     
     for key, model in self.model_dict.items():
         metric_df.at[key, 'f1'] = f1_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
         metric_df.at[key, 'precision'] = precision_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
         metric_df.at[key, 'accuracy'] = accuracy_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
         metric_df.at[key, 'recall'] = recall_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
         
    print(metric_df)
                            
