#################################################
# roc_curves.py
# Methods to create roc curve comparison plots
#
# Last edited on June, 18th, 2018 by David Curry
#
################################################




def roc_curves(self):
    ''' Takes in trained models within a dictionary.
        Outputs a comparison ROC curve plot.
    '''
    plt.figure(figsize = (12, 7))
    axis_font = {"fontname": "DejaVu Sans", "size": "14"}
    title_font = {"fontname": "DejaVu Sans", "size": "16"}
    
    for key, model in self.model_dict.items():
        pred  = model.y_test['probability']
        truth = model.y_test[model.target]
        roc   = roc_auc_score(truth, pred)
        
        pred_t  = model.y_train['probability']
        truth_t = model.y_train[model.target]
        roc_t   = roc_auc_score(truth_t, pred_t)
        
        fpr, tpr, thresholds = roc_curve(truth, pred)
        fpr_t, tpr_t, thresholds_t = roc_curve(truth_t, pred_t)
        
        plt.plot(fpr, tpr, label =  key+" Test (AUC = %0.2f)" % roc)
        plt.plot(fpr_t, tpr_t, label =  key+" Train (AUC = %0.2f)" % roc_t)


    plt.plot([0, 1], [0, 1], "r--")
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel("False Positive", **axis_font)
    plt.ylabel("True Positive", **axis_font)
    plt.title("ROC curve", **title_font)
    plt.xticks(fontsize = 12)
    plt.yticks(fontsize = 12)
    plt.legend(loc = "lower right")
    plt.savefig(self.plot_path+self.version+'_model_roc_comparison.png')
    plt.show()
