##################################################
# model_pipeline.py
# Set of classes to help multi-model processing
#
# Last edited on June 18th by David Curry
#################################################


class model_pipeline():
    ''' 
    Class to define a dictionary of models.
    Inherits dataset from data_Pipeline.
    Efficiently trains and validates on the model dictionary.
    '''

    def __init__(self, data_pipeline, model_list, plot_path='', weight_path='', version=''):
        self.plot_path = plot_path
        self.weight_path = weight_path
        self.version = version
        self.data = data_pipeline
        self.model_list = model_list
        
        self.model_dict = {}
        for model in self.model_list:
            self.model_dict[model[0]] = my_model(self.data, model[1], model[2], self.weight_path, self.plot_path, self.version, model[0])
                
                
    def train_all_classifiers(self):
        ''' Train all models in the model dict'''
        for key,val in self.model_dict.items():
            val.train_classifier()
                
                
    def show_model_performance(self):
        self.roc_curves()
        self.model_metrics_classification()
            
                    
    def roc_curves(self):
        ''' Takes in trained models test.
        Outputs a comparison ROC curve plot.
        '''

        plt.figure(figsize = (12, 7))
        axis_font = {"fontname": "DejaVu Sans", "size": "14"}
        title_font = {"fontname": "DejaVu Sans", "size": "16"}
        
        for key, model in self.model_dict.items():
            pred  = model.y_test['probability']
            truth = model.y_test[model.target]
            roc   = roc_auc_score(truth, pred)
            
            pred_t  = model.y_train['probability']
            truth_t = model.y_train[model.target]
            roc_t   = roc_auc_score(truth_t, pred_t)
            
            fpr, tpr, thresholds = roc_curve(truth, pred)
            fpr_t, tpr_t, thresholds_t = roc_curve(truth_t, pred_t)
            
            plt.plot(fpr, tpr, label =  key+" Test (AUC = %0.2f)" % roc)
            plt.plot(fpr_t, tpr_t, label =  key+" Train (AUC = %0.2f)" % roc_t)
            
                
        plt.plot([0, 1], [0, 1], "r--")
        plt.xlim([0, 1])
        plt.ylim([0, 1])
        plt.xlabel("False Positive", **axis_font)
        plt.ylabel("True Positive", **axis_font)
        plt.title("ROC curve", **title_font)
        plt.xticks(fontsize = 12)
        plt.yticks(fontsize = 12)
        plt.legend(loc = "lower right")
        plt.savefig(self.plot_path+self.version+'_model_roc_comparison.png')
        plt.show()
        
    def model_metrics_classification(self):
        ''' Takes in a dict of models and creates a metric table'''
        
        # DF used for table visualizing
         metric_df = pd.DataFrame(index=[list(self.model_dict.keys())],
                                  columns=['f1', 'precision', 'accuracy', 'recall'])
         
         f1, prec, acc, rec = [], [], [], []
         
         for key, model in self.model_dict.items():
             metric_df.at[key, 'f1'] = f1_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
             metric_df.at[key, 'precision'] = precision_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
             metric_df.at[key, 'accuracy'] = accuracy_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
             metric_df.at[key, 'recall'] = recall_score(model.y_test[model.target], model.y_test['prediction']) * 100.0
             
        print(metric_df)
                            
class my_model():
    ''' 
    Class to wrap up individual models.
    Used by mdoel_pipeline to fill the model dictionary.
    '''

    def __init__(self, data_pipeline, features, target, weight_path, plot_path, version, name):
        
        self.weight_path = weight_path
        self.plot_path = plot_path
        self.title = version+'_'+name
        self.data = data_pipeline
        self.features = features
        self.target = target
        
        
        def train_rf_classifier(self):
            ''' Train and save weights for rf binary classifier.
                This is an example of a method for a given type of classifer.
            
            Inputs
            ------
            classifier_type :
            what type of classifier
            
            Returns
            -------
             Trained Model
            '''

            print("\nTraining Classifer: ", self.title)

            rf_params = {"n_estimators": 100,  # 300
                         "max_depth": 6,      # 30
                         "random_state": 0,
                         #'min_samples_split': 10,
                         "verbose": 0,
                         "n_jobs": -1,
                         "criterion": "gini",
                         "class_weight": {1:.89, 0:.11}}
            

            self.model = RandomForestClassifier(**rf_params).fit(self.data.X_train[self.features], self.data.y_train)
            
            print("Saving model weights...")
            
            pickle.dump(self.model, open(self.weight_path+self.title+'_random_forest.pickle', 'wb'))
            
            print('Making predicitions...')
            
            self.y_train = pd.DataFrame({self.target: self.data.y_train.values}, index=self.data.y_train.index)
            self.y_test  = pd.DataFrame({self.target: self.data.y_test.values}, index=self.data.y_test.index)
            
            # 0/1 predictions
            self.y_train['prediction'] = self.model.predict(self.data.X_train[self.features])
            self.y_test['prediction'] = self.model.predict(self.data.X_test[self.features])
            
            # 0-1 probability score
            self.y_train['probability'] = self.model.predict_proba(self.data.X_train[self.features])[::,1]
            self.y_test['probability']  = self.model.predict_proba(self.data.X_test[self.features])[::,1]



class CV2_Classifier_Data_Pipeline():
    ''' Class to wrap up data processing tasks.
        Each model will absorb the train/test data from this class.
    '''

    def __init__(self, pos_df, neg_df, plot_path='', weight_path='', version=''):
        
        self.plot_path = plot_path
        self.weight_path = weight_path
        self.version = version
        self.pos_df = pos_df
        self.neg_df = neg_df
        


    def set_target(self, target_name):
        ''' Sets the target column name '''
        self.target = target_name
        
    def num_events(self):
        ''' # of events in all journeys'''
        num_pos = self.pos_df.groupby(self.pos_df.index).size().sum()
        num_neg = self.neg_df.groupby(self.neg_df.index).size().sum()
        return num_pos, num_neg
    
    def num_customers(self):
        ''' # of customers present in +/- dataframes'''
        pos = len(self.pos_df['customer_ID'].unique())
        neg = len(self.neg_df['customer_ID'].unique())
        return pos, neg

    def create_test_train(self, percent):
        ''' Create the Test/train dataset split by unique customer
        Inputs
        ------
        percent :
        percent of data set to split into training set.

        Returns
        -------
        test/train dataframes
        '''

        if os.path.exists(self.weight_path+self.version+"_X_train.pickle"):
            print('\nLoading train/test timelines')
            self.X_train = pd.read_pickle(self.weight_path+self.version+"_X_train.pickle")
            self.X_test  = pd.read_pickle(self.weight_path+self.version+"_X_test.pickle")
            self.y_train = pd.read_pickle(self.weight_path+self.version+"_y_train.pickle")
            self.y_test  = pd.read_pickle(self.weight_path+self.version+"_y_test.pickle")
            
            del self.pos_df
            del self.neg_df
        
        else:
            
            print('\nCreating test/train datasets...')

            # Get unique customer IDs
            idx_u = self.df.index.unique(level='customer_id')
            
            # Create array of train and test indices
            np.random.seed(42)
            train_i = np.random.choice(idx_u, int(len(idx_u) * percent), replace=False)
            test_i = idx_u[~idx_u.isin(train_i)]
            
            # Split data
            train = self.df.loc[train_i].fillna(0)
            test  = self.df.loc[test_i].fillna(0)
            
            self.X_train, self.y_train = train.loc[:, x_cols], train[self.target]
            self.X_test, self.y_test   = test.loc[:, x_cols], test[self.target]

            self.X_train.to_pickle(self.weight_path+self.version+'_X_train.pickle')
            self.X_test.to_pickle(self.weight_path+self.version+'_X_test.pickle')
            self.y_train.to_pickle(self.weight_path+self.version+'_y_train.pickle')
            self.y_test.to_pickle(self.weight_path+self.version+'_y_test.pickle')
            
            del self.df

        print('\nTotal Customers:')
        print('Train :', len(self.X_train.index.unique(level=0)))
        print('Test  :', len(self.X_test.index.unique(level='customer_id')))
        
        print("\nTotal # of Training Timelines: ")
        print('+ Train : ', round(0.18*self.X_train.reset_index().groupby('customer_id')['cumsum'].nunique().sum()))
        print('+ Test  : ', round(0.18*self.X_test.reset_index().groupby('customer_id')['cumsum'].nunique().sum()))
        print('- Train : ', round(0.82*self.X_train.reset_index().groupby('customer_id')['cumsum'].nunique().sum()))
        print('- Test  : ', round(0.82*self.X_test.reset_index().groupby('customer_id')['cumsum'].nunique().sum()))
        
        print("\nTotal # of Training Events (rows): ")
        print('+ Train :', round(0.18*len(self.X_train)))
        print('- Test  :', round(0.82*len(self.X_test)))
        
