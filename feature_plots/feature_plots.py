###################################################
# feature_plots.py
# Modules to plot input features
#
# Last edited by David Curry on June 18th, 2018
####################################################



def plot_features_two_classes(df1, df2, features, label1, label2, xlabel=None, ylabel=None, nbins=None, xrange=None, ylog=False, path=None):
    ''' Plots feature distrubtions for binary class comparison.
    ==== Input ====
    df1/df2 : dataframes of two classes to comapre
    label1/2 : class names
    features: list of features to plot
    '''

    # Create 'for loop' to enerate the features and compare with histograms
    for i,b in enumerate(features):
        
        fig = plt.figure()
        fig.set_size_inches(10, 8)
        
        plt.hist(df1[b], label = label1, stacked = True, alpha=0.7, density=True, bins=nbins, range=xrange, histtype='step', linewidth=3)
        plt.hist(df2[b], label= label2, stacked = True, alpha=0.7, density=True, bins=nbins, range=xrange, histtype='step', linewidth=3)
        
        plt.title(b)
        plt.xlabel(b)

        plt.ylabel('density')
        if ylog: plt.yscale('log')
        
        plt.tight_layout()
        plt.legend()
        if path: plt.savefig(path+b+'.png')
        plt.show()



        
